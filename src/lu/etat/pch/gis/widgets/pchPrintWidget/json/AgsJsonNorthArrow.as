package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    public class AgsJsonNorthArrow extends AgsJsonSymbol {

        public function AgsJsonNorthArrow(size:Number=85,fontFamily:String="ESRI North",charIndex:Number=200) {
            type="pchNorthArrow";
			this.fontFamily=fontFamily;
			this.charIndex=charIndex;
			this.size=size;
        }

		private var _fontFamily:String;
		private var _charIndex:Number;
		private var _size:Number;
		private var _backgroundColor:AgsJsonRGBColor;
		
		

		public function get fontFamily():String
		{
			return _fontFamily;
		}

		public function set fontFamily(value:String):void
		{
			_fontFamily = value;
		}

		public function get charIndex():Number
		{
			return _charIndex;
		}

		public function set charIndex(value:Number):void
		{
			_charIndex = value;
		}

        public function get size():Number {
            return _size;
        }

        public function set size(value:Number):void {
            _size=value;
        }

		public function get backgroundColor():AgsJsonRGBColor
		{
			return _backgroundColor;
		}

		public function set backgroundColor(value:AgsJsonRGBColor):void
		{
			_backgroundColor = value;
		}

    }
}