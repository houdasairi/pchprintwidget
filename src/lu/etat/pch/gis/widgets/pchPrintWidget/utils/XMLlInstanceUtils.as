package lu.etat.pch.gis.widgets.pchPrintWidget.utils
{
	import com.esri.ags.geometry.Geometry;
	import com.esri.ags.geometry.MapPoint;
	
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLDocument;
	
	import lu.etat.pch.gis.widgets.pchPrintWidget.json.AgsJsonElement;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.xml.SimpleXMLDecoder;
	import mx.utils.ObjectProxy;

	public class XMLlInstanceUtils
	{
		private var decoder:SimpleXMLDecoder;
		public function XMLlInstanceUtils() {
			decoder=new SimpleXMLDecoder(true);
		}
		
		public function parseCreateLayoutElements(xmlDocument:XMLDocument,dynFieldsHolder:ArrayCollection):ArrayCollection {
			var preloadedElementsObj:Object=decoder.decodeXML(xmlDocument);
			var elementsListDataProvider:ArrayCollection=new ArrayCollection();
			var tmpPreloadElementList:ArrayCollection = new ArrayCollection();
			if(preloadedElementsObj.PreloadedElement is ObjectProxy) {
				tmpPreloadElementList.addItem(preloadedElementsObj.PreloadedElement);
			}else {
				tmpPreloadElementList.addAll(preloadedElementsObj.PreloadedElement);
			}
			
			for(var i:int=0; i<tmpPreloadElementList.length; i++) {
				var tmpObj:Object = tmpPreloadElementList.getItemAt(i);
				if(tmpObj.Symbol) {
				var symbolInstance:*=createInstance(tmpObj.Symbol.classType, tmpObj.Symbol.Attribute,dynFieldsHolder);
				var geomInstance:Geometry = null;
				if(tmpObj.Geometry) {
					geomInstance = Geometry(createInstance(tmpObj.Geometry.classType, tmpObj.Geometry.Attribute,dynFieldsHolder));
				}
				var element:AgsJsonElement=new AgsJsonElement(geomInstance, symbolInstance, tmpObj.positionAnchor, tmpObj.positionOffsetX, tmpObj.positionOffsetY, tmpObj.label, true, tmpObj.height, tmpObj.width);
				if(tmpObj.order) element.order=tmpObj.order;
				
				var obj:ObjectProxy=new ObjectProxy(
					{ label:		 tmpObj.label, 
						name:		 tmpObj.label,
						visible:	 tmpObj.visible, 
						element:	 element, 
						height:	 tmpObj.height, 
						width:		 tmpObj.width,
						selected:	 tmpObj.selected,
						editable:   tmpObj.editable}
				);
				if(element.symbol) {
					if(element.symbol.hasOwnProperty("label")) obj.label = obj.label+": "+element.symbol.label;
					if(element.symbol.hasOwnProperty("text")) {
						obj.label = obj.label+": "+element.symbol.text;
					}
				}
				if(tmpObj.Symbol.popup) {
					obj.popupClass=getDefinitionByName(tmpObj.Symbol.popup);
				}
				elementsListDataProvider.addItem(obj);
				}
				
			}
			return elementsListDataProvider;
		}
		
		public function parseCreateMapGrids(xmlDocument:XMLDocument):ArrayCollection {
			var mapGridsObj:Object = decoder.decodeXML(xmlDocument);
			var mapGridsDataProvider:ArrayCollection=new ArrayCollection();
			if(mapGridsObj) {
				if(mapGridsObj.MapGrid is ObjectProxy) {
					var klass2:*=createInstance(mapGridsObj.MapGrid.classType, mapGridsObj.MapGrid.Attribute,null);
					var mapGridElement:AgsJsonElement=new AgsJsonElement(null, klass2);
					var mapGridVisible:Boolean = false;
					if(mapGridsObj.MapGrid.visible) mapGridVisible=mapGridsObj.MapGrid.visible;
					var mapGridEditable:Boolean = false;
					if(mapGridsObj.MapGrid.editable) mapGridEditable=mapGridsObj.MapGrid.editable;
					
					var obj2:ObjectProxy=new ObjectProxy(
						{ label:		 klass2.label, 
							visible:	 true, 
							element:	 mapGridElement, 
							selected:	 mapGridVisible,
							editable:    mapGridEditable,
							popupClass: getDefinitionByName("lu.etat.pch.gis.widgets.pchPrintWidget.components.PopUp_mapGrid")
						}
					);
					mapGridsDataProvider.addItem(obj2);
				}else if(mapGridsObj.MapGrid is ArrayCollection) {
					for(var iii:int=0; iii<mapGridsObj.MapGrid.length; iii++) {
						var tmpObj2:ObjectProxy = mapGridsObj.MapGrid[iii];
						var klass22:*=createInstance(tmpObj2.classType, tmpObj2.Attribute,null);
						var mapGridElement22:AgsJsonElement=new AgsJsonElement(null, klass22);
						var mapGridVisible22:Boolean = false;
						if(tmpObj2.visible) mapGridVisible22=tmpObj2.visible;
						var mapGridEditable22:Boolean = false;
						if(tmpObj2.editable) mapGridEditable22=tmpObj2.editable;
						var obj22:ObjectProxy=new ObjectProxy(
							{ label:		 klass22.label, 
								visible:	 true, 
								element:	 mapGridElement22, 
								selected:	 mapGridVisible22,
								editable:    mapGridEditable22,
								popupClass: getDefinitionByName("lu.etat.pch.gis.widgets.pchPrintWidget.components.PopUp_mapGrid")
							}
						);
						mapGridsDataProvider.addItem(obj22);
					}				
				}
			}
			return mapGridsDataProvider;
		}
		
		public function createInstance(className:String, args:Object,dynFieldsHolder:ArrayCollection=null):Object {
			var myClass:Class = getDefinitionByName(className) as Class;
			var instance:* = new myClass();
			var ac:ArrayCollection = new ArrayCollection();
			if(args is ArrayCollection) {
				ac.addAll(ArrayCollection(args));
			}else {
					ac.addItem(new ObjectProxy(args));
			}
			
			for each (var item:Object in ac) {
				if(item is ObjectProxy) {
					var o:ObjectProxy = ObjectProxy(item);
					try {
						if (o.classType) {
							var tmpObj:Object =createInstance(o.classType, o.Attribute,dynFieldsHolder)
							if(instance is Array) {
								var tmpArray:Array= instance;
								tmpArray.push(tmpObj);
							}else{
								instance[o.name]=tmpObj;
							}
							
						} else {
							if(o.dynamic) {
								instance[o.name]=null;
								if(dynFieldsHolder){
									var dynHolder:Object=new Object;
									dynHolder.target=instance;
									dynHolder.fieldName = o.name;
									dynHolder.valueExp = o.value
									dynFieldsHolder.addItem(dynHolder);
								}
							}else{
								instance[o.name]=o.value;
							}
						}
					} catch (erObject:Error) {
						Alert.show("The attribute '"+o.name+"' does not exists for '"+className+"'","PrintWidget-XMLInstance");
					}
				}
			}
		/*
					try {
						if(instance is ArrayCollection)
							(instance as ArrayCollection).addItem(op.value);
						else if(instance is Array)
							(instance as Array).push( createInstance(op.classType, op.Attribute));
						else instance[op.name]=op.value; 	
					}catch (erObject:Error) {
						Alert.show("The attribute '"+op.name+"' does not exists: "+erObject.message,"PrintWidget-XMLInstance");
					}
					try {
						if (op.classType)
							if(instance is ArrayCollection)
								(instance as ArrayCollection).addItem( createInstance(op.classType, op.Attribute) );
							else instance[op.name]=createInstance(op.classType, op.Attribute);
						
					}catch (erObject:Error) {
						Alert.show("The attribute '"+op.name+"' does not exists: "+erObject.message,"PrintWidget-XMLInstance");
					}
					*/
			return instance;
		}
		
		private static function getClass(obj:Object):Class {
			return Class(getDefinitionByName(getQualifiedClassName(obj)));
		}
	}
}