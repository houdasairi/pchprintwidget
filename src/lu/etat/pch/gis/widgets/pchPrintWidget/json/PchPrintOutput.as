package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    /**
     * 
     * @author schullto
     */
    public class PchPrintOutput {
        /**
         * 
         * @param height
         * @param width
         * @param format
         * @param resolution
         * @param borderWidth
         * @param mapRotation
         * @param toRemoveLayoutElements
         * @param pageUnits
         * @param exportSettings
         */
        public function PchPrintOutput(height:Number, width:Number, format:String, resolution:Number, borderWidth:Array, mapRotation:Number=0, toRemoveLayoutElements:Array=null, pageUnits:String="cm", exportSettings:PchPrintExportPDF=null,referenceScale:Number=0) {
            this.height=height;
            this.width=width;
            this.resolution=resolution;
            this.format=format;
            this.borderWidth=borderWidth;
            this.mapRotation=mapRotation;
            this.toRemoveLayoutElements=toRemoveLayoutElements;
            this.pageUnits=pageUnits;
            this.exportSettings=exportSettings;
			this.referenceScale=referenceScale;
        }

        private var _borderWidth:Array; // {0,0,0,0} -> {left,right,top,bottom}
        private var _exportSettings:PchPrintExportPDF;
        private var _format:String
        private var _height:Number;
        private var _mapRotation:Number
        private var _pageUnits:String;
        private var _resolution:Number;
        private var _toRemoveLayoutElements:Array;
        private var _width:Number;
		private var _referenceScale:Number
		private var _refererWebsite:String;
		private var _border:AgsJsonSimpleLineSymbol;

        /**
         * 
         * @return 
         */
        public function get borderWidth():Array {
            return _borderWidth;
        }

        /**
         * 
         * @param value
         */
        public function set borderWidth(value:Array):void {
            _borderWidth=value;
        }

        /**
         * 
         * @return 
         */
        public function get exportSettings():PchPrintExportPDF {
            return _exportSettings;
        }

        /**
         * 
         * @param value
         */
        public function set exportSettings(value:PchPrintExportPDF):void {
            _exportSettings=value;
        }

        /**
         * 
         * @return 
         */
        public function get format():String {
            return _format;
        }

        /**
         * 
         * @param value
         */
        public function set format(value:String):void {
            _format=value;
        }

        /**
         * 
         * @return 
         */
        public function get height():Number {
            return _height;
        }

        /**
         * 
         * @param value
         */
        public function set height(value:Number):void {
            _height=value;
        }

        /**
         * 
         * @return 
         */
        public function get mapRotation():Number {
            return _mapRotation;
        }

        /**
         * 
         * @param value
         */
        public function set mapRotation(value:Number):void {
            _mapRotation=value;
        }

        /**
		 * esriUnknownUnits = 0;
		 * inches = 1;
		 * points = 2;
		 * feet = 3;
		 * yards = 4;
		 * miles = 5;
		 * esriNauticalMiles = 6; -> nm
		 * mm = 7;
		 * cm = 8;
		 * m = 9;
		 * km = 10;
		 * esriDecimalDegrees = 11; -> dd
		 * esriDecimeters = 12; -> dm
		 * esriUnitsLast = 13;
         * @return 
         */
        public function get pageUnits():String {
            return _pageUnits;
        }

        /**
		 * esriUnknownUnits = 0;
		 * inches = 1;
		 * points = 2;
		 * feet = 3;
		 * yards = 4;
		 * miles = 5;
		 * esriNauticalMiles = 6; -> nm
		 * mm = 7;
		 * cm = 8;
		 * m = 9;
		 * km = 10;
		 * esriDecimalDegrees = 11; -> dd
		 * esriDecimeters = 12; -> dm
         * @param value
         */
        public function set pageUnits(value:String):void {
            _pageUnits=value;
        }

        /**
         * 
         * @return 
         */
        public function get resolution():Number {
            return _resolution;
        }

        /**
         * 
         * @param value
         */
        public function set resolution(value:Number):void {
            _resolution=value;
        }

        /**
         * 
         * @return 
         */
        public function get toRemoveLayoutElements():Array {
            return _toRemoveLayoutElements;
        }

        /**
         * 
         * @param value
         */
        public function set toRemoveLayoutElements(value:Array):void {
            _toRemoveLayoutElements=value;
        }

        /**
         * 
         * @return 
         */
        public function get width():Number {
            return _width;
        }

        /**
         * 
         * @param value
         */
        public function set width(value:Number):void {
            _width=value;
        }

		/**
		 * 
		 * @return 
		 * 
		 */		
		public function get referenceScale():Number
		{
			return _referenceScale;
		}

		/**
		 * 
		 * @param value
		 *   value = -1 -> set referenceScale to currentPrintScale
		 *   value >= 0 -> set referenceScale to <value> 
		 * 
		 */
		public function set referenceScale(value:Number):void
		{
			_referenceScale = value;
		}

		public function get refererWebsite():String
		{
			return _refererWebsite;
		}

		public function set refererWebsite(value:String):void
		{
			_refererWebsite = value;
		}

		public function get border():AgsJsonSimpleLineSymbol
		{
			return _border;
		}

		public function set border(value:AgsJsonSimpleLineSymbol):void
		{
			_border = value;
		}
		
		
    }
}